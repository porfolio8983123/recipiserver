const express = require('express');
const router = express.Router();
const authController = require('./../controllers/authController');
const userController = require('./../controllers/userController');
const multer = require('multer');
const path = require('path');

const storage = multer.diskStorage({
    destination: function (req,file,cb) {
        cb(null,path.join(__dirname, '../public/profile'));
    },
    filename: function (req,file, cb) {
        const name = Date.now()+"-"+file.originalname;
        cb(null,name);
    }
})

const upload = multer({storage: storage});

router.post('/signup',authController.signup);
router.post('/login',authController.login);
router.post('/sendMail',authController.sendMail);
router.post('/resetPassword',authController.resetPassword);

router
    .route('/:id')
    .patch(userController.updateUser);

router.patch('/:id/image', upload.single('image'), userController.updateUserWithImage);

router.patch('/:id/hasImage',userController.updateHavingImage);

module.exports = router;