const express = require('express');
const router = express.Router();
const viewsController = require('./../controllers/viewController');

router.get('/login',viewsController.getLoginForm);
router.get('/adminLogin',viewsController.getAdminLogin);
router.get('/resetPassword',viewsController.getResetPassword);

module.exports = router;