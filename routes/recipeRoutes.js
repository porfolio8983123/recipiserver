const express = require('express');
const router = express.Router();
const recipeController = require('../controllers/recipeController');
const multer = require('multer');
const path = require('path');
const commentController=require('../controllers/commentController');
const ratingController=require('../controllers/ratingController');


const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.join(__dirname, '../public/recipeImage'));
    },
    filename: function (req, file, cb) {
      const name= Date.now()+"-"+file.originalname;
      cb(null, name);
    }
});

const upload = multer({ storage: storage });

router.post('/upload',upload.single('image'), recipeController.createRecipe);

router.get('/allRecipe',recipeController.getAllRecipes);
router.get('/:id',recipeController.getRecipe);
router.get('/user/:userId',recipeController.getUserRecipe);

router.delete('/:id',recipeController.deleteRecipe);

router.post('/comment',commentController.addComment)
router.post('/rating',ratingController.createRating)

module.exports = router;
