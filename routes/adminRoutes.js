const express = require('express');
const router = express.Router();
const adminController = require('./../controllers/adminController');

router.post('/addAdmin',adminController.addAdmin);
router.post('/loginAdmin',adminController.adminLogin);
router.get('/allAdmin',adminController.getAllAdmins);
router.get('/allUser',adminController.getAllUsers);
router.post('/sendMail',adminController.sendMail);
router.post("/resetPassword",adminController.resetPassword);

router
    .route('/:id')
    .delete(adminController.deleteAdmin);

    router
    .route('/deleteUser/:id')
    .delete(adminController.deleteUser);

module.exports = router;