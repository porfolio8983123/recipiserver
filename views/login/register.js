     
const form = document.querySelector("form");
const nField = form.querySelector(".name");
const nInput = nField.querySelector("input");
const eField = form.querySelector(".email");
const eInput = eField.querySelector("input");
const pField = form.querySelector(".password");
const pInput = pField.querySelector("input");
const cpField = form.querySelector(".cp");
const cpInput = cpField.querySelector("#cpinput");

form.addEventListener("submit", async function (e) {
  e.preventDefault();

  let isEmailValid = checkEmail();
  let isPasswordValid = checkPass();
  let isNameValid = checkName();
  let isConfirmed = confirmPass();

  let isFormValid = isEmailValid && isPasswordValid && isNameValid && isConfirmed;
  if (isFormValid) {
    console.log("test");
    const formData = {
      name: nInput.value,
      email: eInput.value,
      password: pInput.value,
      passwordConfirm: cpInput.value
    };

    await fetch("http://localhost:4001/api/users/signup",{
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    })
    .then(async (response) => {
      const data = await response.json();
      console.log("response ",data);

      if (data.status === 'success') {
        console.log("success");

        window.location.href = "login.html";
      }
    })
    .catch((error) => {
      console.log("error ",error);
    })

  }
});

form.onsubmit = (e) => {
  e.preventDefault(); //preventing from form submitting

  //if email and password is blank then add shake class in it else call specified function
  nInput.value == "" ? nField.classList.add("shake", "error") : checkName();
  eInput.value == "" ? eField.classList.add("shake", "error") : checkEmail();
  pInput.value == "" ? pField.classList.add("shake", "error") : checkPass();
  cpInput.value == "" ? cpField.classList.add("shake", "error") : confirmPass();

  setTimeout(() => {
    //remove shake class after 500ms
    nField.classList.remove("shake");
    eField.classList.remove("shake");
    pField.classList.remove("shake");
    cpField.classList.remove("shake");
  }, 500);

  eInput.onkeyup = () => {
    checkEmail();
  }; //calling checkEmail function on email input keyup
  pInput.onkeyup = () => {
    checkPass();
  };
  // calling checkPassword function on pass input keyup
  cpInput.onkeyup = () => {
    confirmPass();
  }; //calling confirmPass function on pass input keyup
  nInput.onkeyup = () => {
    checkName();
  };
};

function checkEmail() {
  //checkEmail function
  let pattern = /^[^ ]+@[^ ]+\.[a-z]{2,3}$/; //pattern for validate email
  if (!eInput.value.match(pattern)) {
    //if pattern not matched then add error and remove valid class
    eField.classList.add("error");
    eField.classList.remove("valid");
    let errorTxt = eField.querySelector(".error-txt");
    //if email value is not empty then show please enter valid email else show Email can't be blank
    eInput.value != ""
      ? (errorTxt.innerText = "Enter a valid email address")
      : (errorTxt.innerText = "Email can't be blank");
    return false;
  } else {
    //if pattern matched then remove error and add valid class
    eField.classList.remove("error");
    eField.classList.add("valid");
    return true;
  }
}

function checkPass() {
  let isPasswordValid = /^(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9!@#$%^&*]{8,}$/;
  if (!pInput.value.match(isPasswordValid)) {
    //if password not matched then add error and remove valid class
    pField.classList.add("error");
    pField.classList.remove("valid");
    let errorTxt = pField.querySelector(".error-txt");
    //if password value is not empty then show please enter valid password else show Password can't be blank
    pInput.value != ""
      ? (errorTxt.innerText =
          "Password must be at least 8 characters that include at least 1 lowercase character, 1 uppercase characters, 1 number and 1 special character")
      : (errorTxt.innerText = "Password can't be blank");
    return false;
  } else {
    //if pass is empty then remove error and add valid class
    pField.classList.remove("error");
    pField.classList.add("valid");
    return true;
  }
}

function confirmPass() {
  let confirm = pInput.value;
  if (cpInput.value !== confirm) {
    //if password not matched then add error and remove valid class
    cpField.classList.add("error");
    cpField.classList.remove("valid");
    let errorTxt = cpField.querySelector(".error-txt");
    //if password value is not empty then show please enter valid password else show Password can't be blank
    cpInput.value != ""
      ? (errorTxt.innerText = "Password does not match")
      : (errorTxt.innerText = "Confirm Password can't be blank");
    return false;
  } else {
    //if pass is empty then remove error and add valid class
    cpField.classList.remove("error");
    cpField.classList.add("valid");
    return true;
  }
}

function checkName() {
  if (nInput.value.trim() === "") {
    nField.classList.add("error");
    let errorTxt = nField.querySelector(".error-txt");
    errorTxt.innerText = "Name can't be blank";
    return false; // Name is invalid
  } else {
    nField.classList.remove("error");
    let errorTxt = nField.querySelector(".error-txt");
    errorTxt.innerText = ""; // Clear error message
    return true; // Name is valid
  }
}

// JavaScript to toggle password visibility
// JavaScript to toggle password visibility
const togglePassword = document.querySelectorAll('.toggle-password');

togglePassword.forEach(function(icon) {
    icon.addEventListener('click', function() {
        const passwordField = icon.previousElementSibling;
        const type = passwordField.getAttribute('type') === 'password' ? 'text' : 'password';
        passwordField.setAttribute('type', type);
        icon.classList.toggle('fa-eye-slash');
        icon.classList.toggle('fa-eye');
    });
});

// JavaScript to toggle confirm password visibility
const toggleConfirmPassword = document.querySelectorAll('.toggle-confirm-password');

toggleConfirmPassword.forEach(function(icon) {
    icon.addEventListener('click', function() {
        const confirmPasswordField = icon.previousElementSibling;
        const type = confirmPasswordField.getAttribute('type') === 'password' ? 'text' : 'password';
        confirmPasswordField.setAttribute('type', type);
        icon.classList.toggle('fa-eye-slash');
        icon.classList.toggle('fa-eye');
    });
});
