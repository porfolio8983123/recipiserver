const app = require('./app');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config();

const port = 4001;

const local_DB = process.env.DATABASE_LOCAL;

const DB = process.env.DATABASE.replace(
    'PASSWORD',
    process.env.DATABASE_PASSWORD
)

mongoose.connect(DB)
.then((con) => {
    console.log("DB connection successfull");
})
.catch((error) => {
    console.log("error ",error);
})

app.listen(port,() => {
    console.log("App running");
})