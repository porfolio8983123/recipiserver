const express = require('express');
const app = express();
const path = require('path');
const userRouter = require('./routes/userRoutes');
const viewRouter = require('./routes/viewRoutes');
const adminRouter = require('./routes/adminRoutes');

const cors = require('cors');
const recipeRouter= require('./routes/recipeRoutes')

app.use(cors());
app.use('/public',express.static(path.join(__dirname,'public')))
app.use(express.static(path.join(__dirname,'views')))
app.use(express.json());

app.use('/api/users',userRouter);
app.use('/api/recipe',recipeRouter);
app.use('/api/admin',adminRouter);
app.use('/',viewRouter);

module.exports = app;