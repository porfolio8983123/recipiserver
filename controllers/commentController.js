const Comment = require('../models/commentModel');
const Recipe = require('../models/recipeModel');

exports.addComment = async (req, res) => {
    const { userId, recipeId, content } = req.body;
    try {
        // Use the create method to add the comment
        const newComment = await Comment.create({
            userId,
            recipeId,
            content
        });

        // Optionally, add the comment ID to the recipe document
        await Recipe.findByIdAndUpdate(recipeId, {
            $push: { comments: newComment._id }
        });

        res.status(201).json({
            status: 'success',
            data: newComment
           });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// exports.getAllComments=async(req,res)=>{
//     try {
//         const comment = await Comment.find();
//         res.status(200).json(comment);
//     } catch (error) {
//         console.error("Error:", error.message);
//         res.status(500).json({ error: error.message });
//     }
// }
