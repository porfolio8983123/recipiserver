
const fs = require('fs');
const path = require('path');
const Recipe = require('../models/recipeModel');
const Rating=require('../models/ratingModel');
const Comment =require('../models/commentModel');

exports.createRecipe = async (req, res) => {

    try {
    const {userId, recipeName, description, ingredients, steps } = req.body;

    const recipe = await Recipe.create({
        userId,
        recipeName,
        image: req.file.filename,
        description,
        ingredients,
        steps
    });

    res.status(201).json({
        status: 'success',
        data: recipe
    });
    } catch (error) {
    res.status(500).json({ error: error.message });
    };
};


exports.getAllRecipes = async (req, res) => {
    try {
        const recipes = await Recipe.find().sort({ createdAt: -1 }).populate('userId', 'name');
        res.status(200).json(recipes);
    } catch (error) {
        console.error("Error:", error.message);
        res.status(500).json({ error: error.message });
    }
}

//get recipe for particular user 
exports.getUserRecipe = async (req, res) => {
    
    const userId = req.params.userId;

    try {
        const recipes = await Recipe.find({ userId: userId });
        
        if (!recipes || recipes.length === 0) {
            return res.status(404).json({status: 'error', message: "No recipes found for the specified user ID" });
        }

        res.status(200).json(recipes);
    } catch (error) {
        console.error("Error:", error.message);
        res.status(500).json({ error: error.message });
    }
}


exports.getRecipe = async (req, res) => {
    try {
        // Find the recipe by ID and select only the required fields
        const recipe = await Recipe.findById(req.params.id)
            .select('recipeName image description ingredients steps')
            .populate({
                path: 'comments',
                select: 'text content userId', // Include 'text' field here
                populate: {
                    path: 'userId',
                    select: 'email name image'
                },
                options: {
                    sort: { createdAt: -1 } // Sort comments by createdAt in descending order
                }
            })
            .populate({
                path: 'ratings', // Assuming 'ratings' is the field name
                select: 'text recipeId value',
                populate: {
                    path: 'userId',
                    select: 'name email'
                }
               
            });

        // Check if the recipe exists
        if (!recipe) {
            return res.status(404).json({ error: 'Recipe not found' });
        }

        // Send the recipe details as the response
        res.status(200).json(recipe);
    } catch (error) {
        // Handle any errors
        res.status(400).json({ error: error.message });
    }
};

exports.deleteRecipe= async (req,res)=>{
    
    const recipeId = req.params.id;

    try {
        // Find the recipe
        const recipe = await Recipe.findById(recipeId);

        if (!recipe) {
            return res.status(404).json({ error: 'Recipe not found' });
        }

        // Delete the image file associated with the recipe
        const imagePath = path.join(__dirname, '..', 'public', 'recipeImage', recipe.image);
        fs.unlink(imagePath, (err) => {
            if (err) {
                console.error(`Failed to delete image file: ${err.message}`);
            }
        });

        // Remove the recipe from the database
        await Recipe.findByIdAndDelete(recipeId);

        // Remove related comments
        await Comment.deleteMany({ recipeId });

        // Remove related ratings
        await Rating.deleteMany({ recipeId });

        res.status(200).json({ message: 'Recipe and related data removed successfully' });
    } catch (err) {
        console.error(err);
        res.status(500).json({ error: 'Something went wrong!' });
    }

}

