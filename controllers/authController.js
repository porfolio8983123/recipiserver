const User = require('./../models/userModel');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const bcrypt = require('bcryptjs');

exports.signup = async (req,res,next) => {
    try {

        const existingUser = await User.findOne({ email: req.body.email });
        if (existingUser) {
            return res.status(400).json({
                status: 'error',
                message: 'User already exists!'
            });
        }

        const newUser = await User.create(req.body);

        const userId = newUser._id;

        const token = jwt.sign({userId},process.env.JWT_SECRET,{
            expiresIn: process.env.JWT_EXPIRES_IN
        })

        res.status(200).json({
            status: 'success',
            token,
            data: {
                newUser
            }
        })

    } catch (error) {
        res.status(500).json({
            err:error.message
        })
    }
}

exports.login = async (req,res,next) => {
    try {
        const {email,password} = req.body;

        if (!email || !password) {
            return res.status(400).json({
                status: 'error',
                message: 'Please provide an email and password!'
            });
        }

        const user = await User.findOne({email}).select('+password')

        if (!user || !(await user.correctPassword(password,user.password))) {
            return res.status(400).json({
                status: 'error',
                message: 'Incorrect email or password!'
            });
        }

        const userId = user._id;

        const token = jwt.sign({userId},process.env.JWT_SECRET,{
            expiresIn: process.env.JWT_EXPIRES_IN
        })

        res.status(200).json({
            status: 'success',
            token,
            data: {
                user
            }
        })

    } catch (error) {
        res.status(500).json({err:error.message});
    }
}

function generateOTP() {
    return Math.floor(1000 + Math.random() * 9000).toString();
  }

exports.sendMail = async (req,res) => {
    try {
        const {email} = req.body;

        const user = await User.findOne({email});

        if (!user) {
            return res.status(400).json({
                status: 'error',
                message: 'Email doesn\' exist!'
            });
        }

        let transporter = nodemailer.createTransport({
            host: process.env.SMTP_HOST,
            port: process.env.SMTP_PORT,
            secure: false,
            auth: {
                user: process.env.SMTP_MAIL,
                pass: process.env.SMTP_PASSWORD
            }
        })

        let otp = generateOTP();

        let mailOptions = {
            from: process.env.SMTP_MAIL,
            to:email,
            subject: "Recipe",
            text: `Your OTP is ${otp}`
        }

        transporter.sendMail(mailOptions,function(error,info) {
            if (error) {
                console.log(error);
            } else {
                console.log("Email sent");
            }
        })

        res.status(200).json({
            status: 'success',
            data: {
                user
            },
            otp: otp
        })

    } catch (error) {
        res.status(500).json({err:error.message});
    }
}

exports.resetPassword = async (req,res,next) => {
    try {
        const {email,password} = req.body;

        const encryptedPassword = await bcrypt.hash(password,12);

        const user = await User.findOneAndUpdate(
            { email },
            {
                password: encryptedPassword,
            },
            {
                new: true,
                runValidators: true,
            }
        );

        if (!user) {
            return res.status(404).json({
                status: 'fail',
                message: 'User not found'
            });
        }

        res.status(200).json({
            status: 'success',
            message: "Updated successfully!"
        })

    } catch (error) {
        res.status(500).json({err:error.message});
    }
}