const Admin = require('./../models/adminModel');
const User = require('./../models/userModel');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const Recipe=require("../models/recipeModel")
const Comment = require('../models/commentModel');
const Rating = require('../models/ratingModel');
const path = require("path");
const fs = require('fs');
const nodemailer = require('nodemailer');

exports.addAdmin = async (req,res,next) => {
    try {
        const existingAdmin = await Admin.findOne({email:req.body.email});
        if (existingAdmin) {
            return res.status(400).json({
                status: 'error',
                message: 'Admin already exists!'
            });
        }
        const newAdmin = await Admin.create(req.body);

        const adminId = newAdmin._id;

        const token = jwt.sign({adminId},process.env.JWT_SECRET,{
            expiresIn: process.env.JWT_EXPIRES_IN
        })

        res.status(200).json({
            status: 'success',
            token,
            data: {
                newAdmin
            }
        })

    } catch (error) {
        res.status(500).json({
            err:error.message
        })
    }
}

exports.adminLogin = async (req, res, next) => {
    try {
        const {email,password} = req.body;

        if (!email || !password) {
            return res.status(400).json({
                status: 'error',
                message: 'Please provide an email and password!'
            });
        }

        const admin = await Admin.findOne({email}).select('+password');

        if (!admin || !(await admin.correctPassword(password,admin.password))) {
            return res.status(400).json({
                status: 'error',
                message: 'Incorrect email or password!'
            });
        }

        const adminId = admin._id;

        const token = jwt.sign({adminId},process.env.JWT_SECRET,{
            expiresIn: process.env.JWT_EXPIRES_IN
        })

        res.status(200).json({
            status: 'success',
            token,
            data: {
                admin
            }
        })
    } catch (error) {
        res.status(500).json({err:error.message});
    }
}

exports.getAllAdmins = async (req, res, next) => {
    try {
        const admins = await Admin.find();
        res.status(200).json({
            status: 'success',
            data: {
                admins
            }
        });
    } catch (error) {
        res.status(500).json({
            err: error.message
        });
    }
};

exports.deleteAdmin = async (req, res, next) => {
    try {
        const admin = await Admin.findByIdAndDelete(req.params.id);
        
        if (!admin) {
            return res.status(404).json({
                status: 'error',
                message: 'No admin found with that ID'
            });
        }

        res.status(200).json({
            status: 'success',
            data: null
        });
    } catch (error) {
        res.status(500).json({
            err: error.message
        });
    }
};

exports.getAllUsers = async (req, res, next) => {
    try {
        const users = await User.find();
        res.status(200).json({
            status: 'success',
            data: {
                users
            }
        });
    } catch (error) {
        res.status(500).json({
            err: error.message
        });
    }
};

exports.deleteUser = async (req, res, next) => {
    try {
        const user = await User.findByIdAndDelete(req.params.id);
        
        if (!user) {
            return res.status(404).json({
                status: 'error',
                message: 'No user found with that ID'
            });
        }

        if (user.image) {
            const previousImagePath = path.join(__dirname, '../public/profile', user.image);
            fs.unlink(previousImagePath, (err) => {
                if (err) {
                    console.error(`Failed to delete previous image: ${err.message}`);
                }
            });
        }

        const recipes = await Recipe.find({ userId: req.params.id });

        for (const recipe of recipes) {
            // Delete the image file associated with the recipe
            const imagePath = path.join(__dirname, '..', 'public', 'recipeImage', recipe.image);
            fs.unlink(imagePath, (err) => {
                if (err) {
                    console.error(`Failed to delete image file: ${err.message}`);
                }
            });

            // Remove the recipe from the database
            await Recipe.findByIdAndDelete(recipe._id);

            // Remove related comments
            await Comment.deleteMany({ recipeId: recipe._id });

            // Remove related ratings
            await Rating.deleteMany({ recipeId: recipe._id });
        }

        res.status(200).json({
            status: 'success',
            data: null
        });
    } catch (error) {
        res.status(500).json({
            err: error.message
        });
    }
};

exports.sendMail = async (req,res) => {
    try {
        const { email } = req.body;

        const user = await Admin.findOne({ email });

        if (!user) {
            return res.status(400).json({
                status: 'error',
                message: 'Email doesn\'t exist!'
            });
        }

        const resetPasswordLink = 'http://localhost:4001/resetPassword';

        let transporter = nodemailer.createTransport({
            host: process.env.SMTP_HOST,
            port: process.env.SMTP_PORT,
            secure: false,
            auth: {
                user: process.env.SMTP_MAIL,
                pass: process.env.SMTP_PASSWORD
            }
        });

        let mailOptions = {
            from: process.env.SMTP_MAIL,
            to: email,
            subject: "Reset Your Password",
            text: `Click on the following link to reset your password: ${resetPasswordLink}`
        };

        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error);
                return res.status(500).json({ status: 'error', message: 'Error sending email' });
            } else {
                console.log("Email sent");
                return res.status(200).json({
                    status: 'success',
                    message: 'Reset password link sent successfully',
                });
            }
        });

    } catch (error) {
        res.status(500).json({ err: error.message });
    }
}

exports.resetPassword = async (req,res,next) => {
    try {
        const {email,password} = req.body;

        const encryptedPassword = await bcrypt.hash(password,12);

        const user = await Admin.findOneAndUpdate(
            { email },
            {
                password: encryptedPassword,
            },
            {
                new: true,
                runValidators: true,
            }
        );

        if (!user) {
            return res.status(404).json({
                status: 'fail',
                message: 'User not found'
            });
        }

        res.status(200).json({
            status: 'success',
            message: "Updated successfully!"
        })

    } catch (error) {
        res.status(500).json({err:error.message});
    }
}