const path = require('path');

exports.getLoginForm = (req,res) => {
    res.sendFile(path.join(__dirname,'../','views/login','login.html'));
}

exports.getAdminLogin = (req,res) => {
    res.sendFile(path.join(__dirname,'../','views/admin','login.html'));
}

exports.getResetPassword = (req,res) => {
    res.sendFile(path.join(__dirname,'../','views/login','resetpass.html'));
}