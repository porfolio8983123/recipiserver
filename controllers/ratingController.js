const Rating = require('../models/ratingModel');
const Recipe=require('../models/recipeModel');

exports.createRating = async (req, res) => {
  const { userId, recipeId, value } = req.body;

  try {
    const existingRating = await Rating.findOne({ userId, recipeId });

    if (existingRating) {
      existingRating.value = value;
      await existingRating.save();
      res.status(200).json({
        status: 'success',
        data: existingRating,
        message: 'Rating updated successfully'
      });
    } else {
      const rating = await Rating.create({
        userId,
        recipeId,
        value
      });

      await Recipe.findByIdAndUpdate(recipeId, {
        $push: { ratings: rating._id }
      });

      res.status(201).json({
        status: 'success',
        data: rating,
        message: 'Rating created successfully'
      });
    }
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};
