const User = require('./../models/userModel');

exports.updateUser = async (req, res, next) => {
    try {
        const userId = req.params.id;
        const { email, name } = req.body;

        const updatedUser = await User.findByIdAndUpdate(
            userId,
            { email, name, image:null },
            { new: true, runValidators: true }
        );

        if (!updatedUser) {
            return res.status(404).json({
                status: 'fail',
                message: 'User not found'
            });
        }

        res.status(200).json({
            status: 'success',
            data: {
                user: updatedUser
            }
        });
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'An error occurred while updating the user',
            error: err.message
        });
    }
};

const fs = require('fs');
const path = require('path');

exports.updateUserWithImage = async (req, res, next) => {
    try {
        const userId = req.params.id;
        const { email, name } = req.body;

        // Find the user to get the current image path
        const user = await User.findById(userId);
        if (!user) {
            if (req.file) {
                // Delete the new uploaded file if user is not found
                fs.unlinkSync(path.join(__dirname, '../public/profile', req.file.filename));
            }
            return res.status(404).json({
                status: 'fail',
                message: 'User not found'
            });
        }

        // Check if there is a previous image and delete it
        if (user.image) {
            const previousImagePath = path.join(__dirname, '../public/profile', user.image);
            fs.unlink(previousImagePath, (err) => {
                if (err) {
                    console.error(`Failed to delete previous image: ${err.message}`);
                }
            });
        }

        // Build the update object
        const updateData = { email, name };
        if (req.file) {
            updateData.image = req.file.filename;
        }

        const updatedUser = await User.findByIdAndUpdate(
            userId,
            updateData,
            { new: true, runValidators: true }
        );

        res.status(200).json({
            status: 'success',
            data: {
                user: updatedUser
            }
        });
    } catch (err) {
        if (req.file) {
            // Delete the new uploaded file if an error occurred
            fs.unlinkSync(path.join(__dirname, '../public/profile', req.file.filename));
        }
        res.status(500).json({
            status: 'error',
            message: 'An error occurred while updating the user',
            error: err.message
        });
    }
};


exports.updateHavingImage = async (req, res, next) => {
    try {
        const userId = req.params.id;
        const { email, name,image } = req.body;

        const updatedUser = await User.findByIdAndUpdate(
            userId,
            { email, name, image },
            { new: true, runValidators: true }
        );

        if (!updatedUser) {
            return res.status(404).json({
                status: 'fail',
                message: 'User not found'
            });
        }

        res.status(200).json({
            status: 'success',
            data: {
                user: updatedUser
            }
        });
    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'An error occurred while updating the user',
            error: err.message
        });
    }
};
