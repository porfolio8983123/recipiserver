const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ratingSchema = new Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: [true, 'User ID is required']
  },
  recipeId: {
    type: mongoose.Schema.Types.ObjectId,
    required: [true, 'Recipe ID is required']
  },
  value: {
    type: Number,
    required: [true,'Rating is required'],
    min: 1,
    max: 5
  }
});

const RatingModel = mongoose.model('Rating', ratingSchema);

module.exports = RatingModel;
