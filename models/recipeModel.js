const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const recipeSchema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: [true, 'User ID is required']
  },
  recipeName: {
    type: String,
    required: [true, 'Recipe name is required']
  },
  image: {
    type: String,
    required: [true, 'Image URL is required']
  },
  description: {
    type: String,
    required: [true, 'Description is required']
  },
  ingredients: {
    type: String,
    required: [true, 'At least one ingredient is required']
  },
  steps: {
    type: [String],
    required: [true, 'At least one step is required']
  },
  comments: [{
    type: Schema.Types.ObjectId,
    ref: 'Comment'
}],
  ratings: [{
    type: Schema.Types.ObjectId,
    ref: 'Rating'
  }]
},{
  timestamps: true
});

const RecipeModel = mongoose.model('Recipe', recipeSchema);

module.exports = RecipeModel;
